<?php

namespace App\Repository;

use App\Entity\EncyclopediaVehicles;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EncyclopediaVehicles|null find($id, $lockMode = null, $lockVersion = null)
 * @method EncyclopediaVehicles|null findOneBy(array $criteria, array $orderBy = null)
 * @method EncyclopediaVehicles[]    findAll()
 * @method EncyclopediaVehicles[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EncyclopediaVehiclesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EncyclopediaVehicles::class);
    }

    // /**
    //  * @return EncyclopediaVehicles[] Returns an array of EncyclopediaVehicles objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EncyclopediaVehicles
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
