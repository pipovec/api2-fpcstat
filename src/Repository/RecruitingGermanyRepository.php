<?php

namespace App\Repository;

use App\Entity\RecruitingGermany;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RecruitingGermany>
 *
 * @method RecruitingGermany|null find($id, $lockMode = null, $lockVersion = null)
 * @method RecruitingGermany|null findOneBy(array $criteria, array $orderBy = null)
 * @method RecruitingGermany[]    findAll()
 * @method RecruitingGermany[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecruitingGermanyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RecruitingGermany::class);
    }

//    /**
//     * @return RecruitingGermany[] Returns an array of RecruitingGermany objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('r.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?RecruitingGermany
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
