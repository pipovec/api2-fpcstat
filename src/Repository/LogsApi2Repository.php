<?php

namespace App\Repository;

use App\Entity\LogsApi2;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LogsApi2|null find($id, $lockMode = null, $lockVersion = null)
 * @method LogsApi2|null findOneBy(array $criteria, array $orderBy = null)
 * @method LogsApi2[]    findAll()
 * @method LogsApi2[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LogsApi2Repository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LogsApi2::class);
    }

    // /**
    //  * @return LogsApi2[] Returns an array of LogsApi2 objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LogsApi2
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
