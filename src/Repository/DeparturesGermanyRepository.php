<?php

namespace App\Repository;

use App\Entity\DeparturesGermany;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DeparturesGermany>
 *
 * @method DeparturesGermany|null find($id, $lockMode = null, $lockVersion = null)
 * @method DeparturesGermany|null findOneBy(array $criteria, array $orderBy = null)
 * @method DeparturesGermany[]    findAll()
 * @method DeparturesGermany[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DeparturesGermanyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DeparturesGermany::class);
    }

//    /**
//     * @return DeparturesGermany[] Returns an array of DeparturesGermany objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('d')
//            ->andWhere('d.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('d.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?DeparturesGermany
//    {
//        return $this->createQueryBuilder('d')
//            ->andWhere('d.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
