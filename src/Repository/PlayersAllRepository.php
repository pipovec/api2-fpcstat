<?php

namespace App\Repository;

use App\Entity\PlayersAll;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PlayersAll|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlayersAll|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlayersAll[]    findAll()
 * @method PlayersAll[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlayersAllRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlayersAll::class);
    }

    // /**
    //  * @return PlayersAll[] Returns an array of PlayersAll objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PlayersAll
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
