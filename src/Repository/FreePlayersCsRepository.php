<?php

namespace App\Repository;

use App\Entity\FreePlayersCs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FreePlayersCs|null find($id, $lockMode = null, $lockVersion = null)
 * @method FreePlayersCs|null findOneBy(array $criteria, array $orderBy = null)
 * @method FreePlayersCs[]    findAll()
 * @method FreePlayersCs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FreePlayersCsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FreePlayersCs::class);
    }

    // /**
    //  * @return FreePlayersCs[] Returns an array of FreePlayersCs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FreePlayersCs
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
