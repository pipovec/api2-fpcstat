<?php

namespace App\Repository;

use App\Entity\ExpectedTankValue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ExpectedTankValue|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExpectedTankValue|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExpectedTankValue[]    findAll()
 * @method ExpectedTankValue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExpectedTankValueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExpectedTankValue::class);
    }

    // /**
    //  * @return ExpectedTankValue[] Returns an array of ExpectedTankValue objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ExpectedTankValue
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
