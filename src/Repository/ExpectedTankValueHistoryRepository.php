<?php

namespace App\Repository;

use App\Entity\ExpectedTankValueHistory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ExpectedTankValueHistory|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExpectedTankValueHistory|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExpectedTankValueHistory[]    findAll()
 * @method ExpectedTankValueHistory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExpectedTankValueHistoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExpectedTankValueHistory::class);
    }

    // /**
    //  * @return ExpectedTankValueHistory[] Returns an array of ExpectedTankValueHistory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ExpectedTankValueHistory
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
