<?php
declare (strict_types = 1);


namespace App\Entity;


use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\ApiProperty;
use App\State\PlayerClansProvider;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(operations: [
        new GetCollection(
            uriTemplate: '/player_clans/{account_id}', uriVariables: ['account_id'], provider: PlayerClansProvider::class
        ),
    ], cacheHeaders: [
        'max_age'        => 86400,
        'shared_max_age' => 86400,
    ], normalizationContext: ['groups' => 'player-clans:read', 'swagger_definition_name' => 'Read'])]
class PlayerClans
{
    #[ApiProperty(identifier: true)]
    public int $clan_id;
    /**
     * Abbreviation of the clan
     */
    #[Groups('player-clans:read')]
    public string $abbreviation;
    /**
     * Full name of the clan
     */
    #[Groups('player-clans:read')]
    public string $name;
    /**
     * Url of the clan image
     */
    #[Groups('player-clans:read')]
    public string $emblem;
    /**
     * Url of the clan small image
     */
    #[Groups('player-clans:read')]
    public string $emblem_small;
    /**
     * Language of the clan
     */
    #[Groups('player-clans:read')]
    public string $language;

    /**
     * @param int    $clan_id
     * @param string $abbreviation
     * @param string $name
     * @param string $emblem
     * @param string $emblem_small
     * @param string $language
     */
    public function __construct(
        int $clan_id,
        string $abbreviation,
        string $name,
        string $emblem,
        string $emblem_small,
        string $language
    ) {
        $this->clan_id = $clan_id;
        $this->abbreviation = $abbreviation;
        $this->name = $name;
        $this->emblem = $emblem;
        $this->emblem_small = $emblem_small;
        $this->language = $language;
    }
}
