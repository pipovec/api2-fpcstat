<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\ExpectedTankValueRepository;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(operations: [
    new Get(uriTemplate: '/expected_tank_values/{tank_id}'),
], cacheHeaders: [
    'max_age'        => 86400,
    'shared_max_age' => 86400,
], paginationEnabled: false)]
#[ORM\Entity(repositoryClass: ExpectedTankValueRepository::class)]
class ExpectedTankValue
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    private $tank_id;

    #[ORM\Column(type: 'float')]
    private $frag;

    #[ORM\Column(type: 'float')]
    private $dmg;

    #[ORM\Column(type: 'float')]
    private $spot;

    #[ORM\Column(type: 'float')]
    private $def;

    #[ORM\Column(type: 'float')]
    private $win;


    public function getTankId(): ?int
    {
        return $this->tank_id;
    }

    public function setTankId(int $tank_id): self
    {
        $this->tank_id = $tank_id;

        return $this;
    }

    public function getFrag(): ?float
    {
        return $this->frag;
    }

    public function setFrag(?float $frag): self
    {
        $this->frag = $frag;

        return $this;
    }

    public function getDmg(): ?float
    {
        return $this->dmg;
    }

    public function setDmg(float $dmg): self
    {
        $this->dmg = $dmg;

        return $this;
    }

    public function getSpot(): ?float
    {
        return $this->spot;
    }

    public function setSpot(float $spot): self
    {
        $this->spot = $spot;

        return $this;
    }

    public function getDef(): ?float
    {
        return $this->def;
    }

    public function setDef(float $def): self
    {
        $this->def = $def;

        return $this;
    }

    public function getWin(): ?float
    {
        return $this->win;
    }

    public function setWin(float $win): self
    {
        $this->win = $win;

        return $this;
    }
}
