<?php

declare (strict_types = 1);

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\EncyclopediaVehiclesRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(paginationItemsPerPage: 100)]
#[ApiFilter(filterClass: SearchFilter::class, properties: [
    'name' => 'partial',
    'tank_id' => 'exact',
    'level'   => 'exact',
    'nation'  => 'exact',
    'type'    => 'exact',
])]
#[Get(uriTemplate: '/encyclopedia_vehicles/{tank_id}')]
#[GetCollection(uriTemplate: '/encyclopedia_vehicles')]
#[ApiFilter(filterClass: DateFilter::class, properties: ['expectedTankValueHistories.date'])]
#[ApiFilter(filterClass: OrderFilter::class, properties: ['expectedTankValueHistories.date' => 'desc'])]
#[ORM\Entity(repositoryClass: EncyclopediaVehiclesRepository::class)]
class EncyclopediaVehicles
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[Groups('encyclopedia:read"')]
    private ?int $tank_id;

    #[ORM\Column(type: 'text')]
    #[Groups('encyclopedia:read"')]
    private ?string $name;

    #[ORM\Column(type: 'integer')]
    #[Groups('encyclopedia:read"')]
    private ?int $level;

    #[ORM\Column(type: 'text')]
    #[Groups('encyclopedia:read"')]
    private ?string $nation;

    #[ORM\Column(type: 'text')]
    #[Groups('encyclopedia:read"')]
    private ?string $tag;

    #[ORM\Column(type: 'text')]
    #[Groups('encyclopedia:read"')]
    private ?string $type;

    #[ORM\Column(type: 'text')]
    #[Groups('encyclopedia:read"')]
    private ?string $big_icon;

    #[ORM\Column(type: 'text')]
    #[Groups('encyclopedia:read"')]
    private ?string $small_icon;

    #[ORM\Column(type: 'text')]
    #[Groups('encyclopedia:read"')]
    private ?string    $contour_icon;
    #[ORM\Column(type: 'boolean')]
    #[Groups('encyclopedia:read"')]
    private ?bool      $is_premium;


    /**
     * @return int|null
     */
    public function getTankId(): ?int
    {
        return $this->tank_id;
    }

    /**
     * @param int $tank_id
     *
     * @return $this
     */
    public function setTankId(int $tank_id): self
    {
        $this->tank_id = $tank_id;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getLevel(): ?int
    {
        return $this->level;
    }

    /**
     * @param int|null $level
     *
     * @return $this
     */
    public function setLevel(?int $level): self
    {
        $this->level = $level;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNation(): ?string
    {
        return $this->nation;
    }

    /**
     * @param string $nation
     *
     * @return $this
     */
    public function setNation(string $nation): self
    {
        $this->nation = $nation;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTag(): ?string
    {
        return $this->tag;
    }

    /**
     * @param string $tag
     *
     * @return $this
     */
    public function setTag(string $tag): self
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBigIcon(): ?string
    {
        return $this->big_icon;
    }

    /**
     * @param string $big_icon
     *
     * @return $this
     */
    public function setBigIcon(string $big_icon): self
    {
        $this->big_icon = $big_icon;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSmallIcon(): ?string
    {
        return $this->small_icon;
    }

    /**
     * @param string $small_icon
     *
     * @return $this
     */
    public function setSmallIcon(string $small_icon): self
    {
        $this->small_icon = $small_icon;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContourIcon(): ?string
    {
        return $this->contour_icon;
    }

    /**
     * @param string $contour_icon
     *
     * @return $this
     */
    public function setContourIcon(string $contour_icon): self
    {
        $this->contour_icon = $contour_icon;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsPremium(): ?bool
    {
        return $this->is_premium;
    }

    /**
     * @param bool $is_premium
     *
     * @return $this
     */
    public function setIsPremium(bool $is_premium): self
    {
        $this->is_premium = $is_premium;

        return $this;
    }
}
