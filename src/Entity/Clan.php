<?php
declare(strict_types = 1);

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\ClanRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Table(name: 'clan_all')]
#[ORM\Entity(repositoryClass: ClanRepository::class)]
#[ApiResource(
    operations: [new Get(), new GetCollection()],
    normalizationContext: [
    'groups'                  => 'read',
    'swagger_definition_name' => 'Read',
])]
#[ApiFilter(filterClass: SearchFilter::class, properties: [
    'clan_id',
    'abbreviation' => 'start',
    'language'     => 'start',
])]
class Clan
{
    #[ORM\Id, ORM\Column(type: 'integer')]
    #[Groups('read')]
    private int $clan_id;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups('read')]
    private string $abbreviation;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[Groups('read')]
    private int    $created_at;

    #[Groups('read')]
    #[ORM\Column(type: 'integer', nullable: true)]
    private int  $owner_id;

    #[Groups('read')]
    #[ORM\Column(type: 'text', nullable: true)]
    private string $name;

    #[Groups('read')]
    #[ORM\Column(type: 'integer', nullable: true)]
    private int  $members_count;

    #[Groups('read')]
    #[ORM\Column(type: 'text', nullable: true)]
    private string $emblems_small;

    #[Groups('read')]
    #[ORM\Column(type: 'text', nullable: true)]
    private string $emblems_tank;

    #[Groups('read')]
    #[ORM\Column(type: 'text', nullable: true)]
    private string $emblems_large;

    #[Groups('read')]
    #[ORM\Column(type: 'text', nullable: true)]
    private string $emblems_medium;

    #[Groups('read')]
    #[ORM\Column(type: 'string', length: 5, nullable: true)]
    private string $language;

    public function getClanId(): ?int
    {
        return $this->clan_id;
    }

    public function setClanId(int $clan_id): self
    {
        $this->clan_id = $clan_id;

        return $this;
    }

    public function getAbbreviation(): ?string
    {
        return $this->abbreviation;
    }

    public function setAbbreviation(?string $abbreviation): self
    {
        $this->abbreviation = $abbreviation;

        return $this;
    }

    public function getCreatedAt(): ?int
    {
        return $this->created_at;
    }

    public function setCreatedAt(?int $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getOwnerId(): ?int
    {
        return $this->owner_id;
    }

    public function setOwnerId(?int $owner_id): self
    {
        $this->owner_id = $owner_id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMembersCount(): ?int
    {
        return $this->members_count;
    }

    public function setMembersCount(?int $members_count): self
    {
        $this->members_count = $members_count;

        return $this;
    }

    public function getEmblemsSmall(): ?string
    {
        return $this->emblems_small;
    }

    public function setEmblemsSmall(?string $emblems_small): self
    {
        $this->emblems_small = $emblems_small;

        return $this;
    }

    public function getEmblemsTank(): ?string
    {
        return $this->emblems_tank;
    }

    public function setEmblemsTank(?string $emblems_tank): self
    {
        $this->emblems_tank = $emblems_tank;

        return $this;
    }

    public function getEmblemsLarge(): ?string
    {
        return $this->emblems_large;
    }

    public function setEmblemsLarge(?string $emblems_large): self
    {
        $this->emblems_large = $emblems_large;

        return $this;
    }

    public function getEmblemsMedium(): ?string
    {
        return $this->emblems_medium;
    }

    public function setEmblemsMedium(?string $emblems_medium): self
    {
        $this->emblems_medium = $emblems_medium;

        return $this;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(?string $language): self
    {
        $this->language = $language;

        return $this;
    }
}
