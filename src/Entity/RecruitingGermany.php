<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Odm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use App\Repository\RecruitingGermanyRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: RecruitingGermanyRepository::class)]
#[ORM\Table(name: 'recruiting_germany')]
#[ApiResource(
    description: 'Free german players',
    operations: [
    new Get(
        uriTemplate: '/recruiting-germany/{account_id}',
        normalizationContext: ['groups' => 'germany:read']
    ),
    new GetCollection(
        uriTemplate: '/recruiting-germany',
        normalizationContext: ['groups' => 'germany:read']
    ),
])]
#[ApiFilter(filterClass: SearchFilter::class, properties: ['nickname' => 'start'])]
#[ApiFilter(filterClass: BooleanFilter::class, properties: ['tank_1', 'tank_2', 'tank_3'])]
#[ApiFilter(filterClass: OrderFilter::class, properties: ['wn8' => 'DESC'], arguments: ['orderParameterName' => 'order'])]
#[ApiFilter(filterClass: RangeFilter::class, properties: ['wn8'])]
class RecruitingGermany
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[Groups('germany:read')]
    #[ApiProperty(description: 'Server account ID')]
    private ?int $account_id = null;

    #[ApiProperty(description: 'Player nick')]
    #[ORM\Column(type: 'string')]
    #[Groups('germany:read')]
    private ?string $nickname = null;

    #[ApiProperty(description: 'Total number of battles')]
    #[ORM\Column(type: 'integer', nullable: true)]
    #[Groups('germany:read')]
    private ?int $battles_all = null;

    #[ApiProperty(description: 'Win rate of player')]
    #[ORM\Column(type: 'float',nullable: true)]
    #[Groups('germany:read')]
    private ?float $win_rate = null;

    #[ApiProperty(description: 'WN8')]
    #[ORM\Column(type: 'float')]
    #[Groups('germany:read')]
    private float $wn8;

    #[ApiProperty(description: 'Global rating')]
    #[ORM\Column(type: 'integer', nullable: true)]
    #[Groups('germany:read')]
    private ?int $global_rating = null;

    #[ApiProperty(description: 'Number of level 10 tanks')]
    #[ORM\Column(name: 'tier_10', type: 'integer', nullable: true)]
    #[Groups('germany:read')]
    private ?int $tier_10 = null;

    #[ApiProperty(description: 'Number of level 8 tanks')]
    #[ORM\Column(name: 'tier_8', type: 'integer', nullable: true)]
    #[Groups('germany:read')]
    private ?int $tier_8 = null;

    #[ApiProperty(description: 'Number of level 6 tanks')]
    #[ORM\Column(name: 'tier_6', type: 'integer', nullable: true)]
    #[Groups('germany:read')]
    private ?int $tier_6 = null;

    #[ApiProperty(description: 'Number of battles in 7 days')]
    #[ORM\Column(name: 'battles_7', type: 'integer', nullable: true)]
    #[Groups('germany:read')]
    private ?int $battles_7 = null;

    #[ApiProperty(description: 'Number of battles in 14 days')]
    #[ORM\Column(name: 'battles_14', type: 'integer', nullable: true)]
    #[Groups('germany:read')]
    private ?int $battles_14 = null;

    #[ApiProperty(description: 'Number of battles in 30 days')]
    #[ORM\Column(name: 'battles_30', type: 'integer', nullable: true)]
    #[Groups('germany:read')]
    private ?int $battles_30 = null;

    #[ApiProperty(description: 'Average XP for battle')]
    #[ORM\Column(type: 'integer', nullable: true)]
    #[Groups('germany:read')]
    private ?int $avg_xp = null;

    #[ApiProperty(description: 'He sometimes had the role of a combat officer')]
    #[ORM\Column(type: 'boolean')]
    #[Groups('germany:read')]
    private bool $combat_officer;

    #[ApiProperty(description: 'He sometimes had the role of a personnel office')]
    #[ORM\Column(type: 'boolean')]
    #[Groups('germany:read')]
    private bool $personnel_officer;


    #[ApiProperty(description: 'Tank Obj.279r')]
    #[ORM\Column(type: 'boolean')]
    #[Groups('germany:read')]
    private bool $tank_1;

    #[ApiProperty(description: 'Tank Obj.907')]
    #[ORM\Column(type: 'boolean')]
    #[Groups('germany:read')]
    private bool $tank_2;

    #[ApiProperty(description: 'T95/FV4201 Chieftain')]
    #[ORM\Column(type: 'boolean')]
    #[Groups('germany:read')]
    private bool $tank_3;


    public function getAccountId(): ?int
    {
        return $this->account_id;
    }

    public function setAccountId(int $account_id): static
    {
        $this->account_id = $account_id;

        return $this;
    }

    public function getNickname(): ?string
    {
        return rtrim($this->nickname, " ");
    }

    public function setNickname(string $nickname): static
    {
        $this->nickname = $nickname;

        return $this;
    }

    public function getBattlesAll(): ?int
    {
        return $this->battles_all;
    }

    public function setBattlesAll(?int $battles_all): static
    {
        $this->battles_all = $battles_all;

        return $this;
    }

    public function getWinRate(): ?float
    {
        return $this->win_rate;
    }

    public function setWinRate(?float $win_rate): static
    {
        $this->win_rate = $win_rate;

        return $this;
    }

    public function getWn8(): float
    {
        return round($this->wn8, 2);
    }

    public function setWn8(float $wn8): static
    {
        $this->wn8 = $wn8;

        return $this;
    }

    public function getGlobalRating(): ?int
    {
        return $this->global_rating;
    }

    public function setGlobalRating(int $global_rating): static
    {
        $this->global_rating = $global_rating;

        return $this;
    }

    public function getTier10(): ?int
    {
        return $this->tier_10;
    }

    public function setTier10(?int $tier_10): static
    {
        $this->tier_10 = $tier_10;

        return $this;
    }

    public function getTier8(): ?int
    {
        return $this->tier_8;
    }

    public function setTier8(?int $tier_8): static
    {
        $this->tier_8 = $tier_8;

        return $this;
    }

    public function getTier6(): ?int
    {
        return $this->tier_6;
    }

    public function setTier6(?int $tier_6): static
    {
        $this->tier_6 = $tier_6;

        return $this;
    }

    public function getBattles7(): ?int
    {
        return $this->battles_7;
    }

    public function setBattles7(?int $battles_7): static
    {
        $this->battles_7 = $battles_7;

        return $this;
    }

    public function getBattles14(): ?int
    {
        return $this->battles_14;
    }

    public function setBattles14(?int $battles_14): static
    {
        $this->battles_14 = $battles_14;

        return $this;
    }

    public function getBattles30(): ?int
    {
        return $this->battles_30;
    }

    public function setBattles30(?int $battles_30): static
    {
        $this->battles_30 = $battles_30;

        return $this;
    }

    public function getAvgXp(): ?int
    {
        return $this->avg_xp;
    }

    public function setAvgXp(?int $avg_xp): static
    {
        $this->avg_xp = $avg_xp;

        return $this;
    }

    public function isCombatOfficer(): bool
    {
        return $this->combat_officer;
    }

    public function setCombatOfficer(bool $combat_officer): static
    {
        $this->combat_officer = $combat_officer;

        return $this;
    }

    public function isPersonnelOfficer(): bool
    {
        return $this->personnel_officer;
    }

    public function setPersonnelOfficer(bool $personnel_officer): static
    {
        $this->personnel_officer = $personnel_officer;

        return $this;
    }

    public function isTank1(): bool
    {
        return $this->tank_1;
    }

    public function setTank1(bool $tank_1): static
    {
        $this->tank_1 = $tank_1;

        return $this;
    }

    public function isTank2(): bool
    {
        return $this->tank_2;
    }

    public function setTank2(bool $tank_2): static
    {
        $this->tank_2 = $tank_2;

        return $this;
    }

    public function isTank3(): bool
    {
        return $this->tank_3;
    }

    public function setTank3(bool $tank_3): static
    {
        $this->tank_3 = $tank_3;

        return $this;
    }
}
