<?php

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(operations: [new Get(), new GetCollection()], normalizationContext: [
    'groups' => [
        'user:read',
        'swagger_definition_name' => 'Read',
    ],
])]
#[ORM\Table(name: '`users`')]
#[ORM\Entity(repositoryClass: UserRepository::class)]
class User
{
    #[ApiProperty(identifier: true)]
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    private int                 $account_id;
    #[Groups(['user:read', 'post:read'])]
    #[ORM\Column(type: 'string', length: 255)]
    private string              $nickname;
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private string              $wg_token;
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?\DateTimeImmutable $nickname_verified_at;
    #[ORM\Column(type: 'string', length: 255)]
    private ?string             $password;
    #[ORM\Column(type: 'string', length: 100, nullable: true)]
    private ?string             $remember_token;
    #[Groups(['user:read'])]
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?\DateTimeImmutable $created_at;
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?\DateTimeImmutable $updated_at;
    #[Groups(['user:read', 'post:read'])]
    #[ORM\Column(type: 'string', length: 10, nullable: true)]
    private ?string             $role;


    public function getAccountId(): ?int
    {
        return $this->account_id;
    }

    public function setAccountId(int $account_id): self
    {
        $this->account_id = $account_id;

        return $this;
    }

    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    public function setNickname(string $nickname): self
    {
        $this->nickname = $nickname;

        return $this;
    }

    public function getWgToken(): ?string
    {
        return $this->wg_token;
    }

    public function setWgToken(?string $wg_token): self
    {
        $this->wg_token = $wg_token;

        return $this;
    }

    public function getNicknameVerifiedAt(): ?\DateTimeImmutable
    {
        return $this->nickname_verified_at;
    }

    public function setNicknameVerifiedAt(?\DateTimeImmutable $nickname_verified_at): self
    {
        $this->nickname_verified_at = $nickname_verified_at;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getRememberToken(): ?string
    {
        return $this->remember_token;
    }

    public function setRememberToken(?string $remember_token): self
    {
        $this->remember_token = $remember_token;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeImmutable $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(?string $role): self
    {
        $this->role = $role;

        return $this;
    }
}
