<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\FreePlayersCsRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(description: 'Volny hraci ktory niekedy boli v Cz/Sk klane.', operations: [
    new Get(uriTemplate: '/free_players_cs/{accountId}'),
    new GetCollection(
        uriTemplate: '/free_players_cs', normalizationContext: ['groups'                  => 'freePlayers:read',
                                                                'swagger_definition_name' => 'Read',
                   ]
    ),
], cacheHeaders: [
    'max_age'        => 3600,
    'shared_max_age' => 3600,
], paginationClientItemsPerPage: true, paginationItemsPerPage: 30)]
#[ApiFilter(filterClass: BooleanFilter::class, properties: ['polniVelitel', 'naborar', 'tank_1', 'tank_2', 'tank_3'])]
#[ApiFilter(filterClass: SearchFilter::class, properties: ['accountName' => 'start'])]
#[ApiFilter(filterClass: OrderFilter::class, properties: [
    'battlesAll',
    'battlesSk',
    'battlesSr',
    'winrate',
    'avg_tier',
    'wn8',
    'global_rating',
    'pocetX',
    'pocet8',
    'pocet6',
    'last7',
    'last14',
    'last30',
    'avgxp',
])]
#[ApiFilter(filterClass: RangeFilter::class, properties: ['wn8'])]
#[ORM\Table('nabor_new')]
#[ORM\Entity(repositoryClass: FreePlayersCsRepository::class)]
class FreePlayersCs
{
    #[Groups('freePlayers:read')]
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    private ?int    $accountId;
    #[Groups('freePlayers:read')]
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $accountName;
    #[ApiProperty(description: 'Pocet bitiek v random')]
    #[Groups('freePlayers:read')]
    #[ORM\Column(type: 'float', nullable: true)]
    private ?float  $battlesAll;
    #[ApiProperty(description: 'Pocet bitiek v sarvatkach')]
    #[Groups('freePlayers:read')]
    #[ORM\Column(type: 'float', nullable: true)]
    private ?float  $battlesSk;
    #[ApiProperty(description: 'Pocet bitiek v stronghold')]
    #[Groups('freePlayers:read')]
    #[ORM\Column(type: 'float', nullable: true)]
    private ?float  $battlesSr;
    #[Groups('freePlayers:read')]
    #[ORM\Column(type: 'float', nullable: true)]
    private         $winrate;
    #[Groups('freePlayers:read')]
    #[ORM\Column(type: 'float', nullable: true)]
    private         $avg_tier;
    #[Groups('freePlayers:read')]
    #[ORM\Column(type: 'float', nullable: true)]
    private ?float  $wn8;
    #[Groups('freePlayers:read')]
    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int    $global_rating;
    #[ApiProperty(description: 'Pocet tankov level 10')]
    #[Groups('freePlayers:read')]
    #[ORM\Column(type: 'integer', name: 'pocet_x', nullable: true)]
    private ?int    $pocetX;
    #[ApiProperty(description: 'Pocet tankov level 8')]
    #[Groups('freePlayers:read')]
    #[ORM\Column(type: 'integer', name: 'pocet_8', nullable: true)]
    private ?int    $pocet8;
    #[ApiProperty(description: 'Pocet tankov level 6')]
    #[Groups('freePlayers:read')]
    #[ORM\Column(type: 'integer', name: 'pocet_6', nullable: true)]
    private ?int    $pocet6;
    #[ApiProperty(description: 'Pocet random bitiek za posldnych 14 dni')]
    #[Groups('freePlayers:read')]
    #[ORM\Column(type: 'float', nullable: true)]
    private ?float  $last14;
    #[ApiProperty(description: 'Pocet random bitiek za posldnych 7 dni')]
    #[Groups('freePlayers:read')]
    #[ORM\Column(type: 'float', nullable: true)]
    private ?float  $last7;
    #[ApiProperty(description: 'Pocet random bitiek za posldnych 30 dni')]
    #[Groups('freePlayers:read')]
    #[ORM\Column(type: 'float', nullable: true)]
    private ?float  $last30;
    #[Groups('freePlayers:read')]
    #[ORM\Column(type: 'float', nullable: true)]
    private ?float  $avgxp;
    #[ApiProperty(description: 'Zastaval niekedy rolu "Polni velitel?"')]
    #[Groups('freePlayers:read')]
    #[ORM\Column(type: 'boolean')]
    private ?bool   $polniVelitel;
    #[ApiProperty(description: 'Zastaval niekedy rolu "Naborar?"')]
    #[Groups('freePlayers:read')]
    #[ORM\Column(type: 'boolean')]
    private ?bool   $naborar;
    #[ApiProperty(description: 'Tank Obj.279r')]
    #[Groups('freePlayers:read')]
    #[ORM\Column(type: 'boolean')]
    private ?bool   $tank_1;
    #[ApiProperty(description: 'Tank Obj.907')]
    #[Groups('freePlayers:read')]
    #[ORM\Column(type: 'boolean')]
    private ?bool   $tank_2;
    #[ApiProperty(description: 'FV 4201 Chieftain')]
    #[Groups('freePlayers:read')]
    #[ORM\Column(type: 'boolean')]
    private ?bool   $tank_3;

    public function getAccountId(): ?int
    {
        return $this->accountId;
    }

    public function setAccountId(int $accountId): self
    {
        $this->accountId = $accountId;

        return $this;
    }

    public function getAccountName(): ?string
    {
        return $this->accountName;
    }

    public function setAccountName(string $accountName): self
    {
        $this->accountName = $accountName;

        return $this;
    }

    public function getBattlesAll(): ?float
    {
        return $this->battlesAll;
    }

    public function setBattlesAll(float $battlesAll): self
    {
        $this->battlesAll = $battlesAll;

        return $this;
    }

    public function getBattlesSk(): ?float
    {
        return $this->battlesSk;
    }

    public function setBattlesSk(float $battlesSk): self
    {
        $this->battlesSk = $battlesSk;

        return $this;
    }

    public function getBattlesSr(): ?float
    {
        return $this->battlesSr;
    }

    public function setBattlesSr(float $battlesSr): self
    {
        $this->battlesSr = $battlesSr;

        return $this;
    }

    public function getWinrate(): ?float
    {
        return $this->winrate;
    }

    public function setWinrate(?float $winrate): self
    {
        $this->winrate = $winrate;

        return $this;
    }

    public function getAvgTier(): ?float
    {
        return $this->avg_tier;
    }

    public function setAvgTier(?float $avg_tier): self
    {
        $this->avg_tier = $avg_tier;

        return $this;
    }

    public function getWn8(): ?float
    {
        return $this->wn8;
    }

    public function setWn8(?float $wn8): self
    {
        $this->wn8 = $wn8;

        return $this;
    }

    public function getGlobalRating(): ?int
    {
        return $this->global_rating;
    }

    public function setGlobalRating(?int $global_rating): self
    {
        $this->global_rating = $global_rating;

        return $this;
    }

    public function getPocetX(): ?int
    {
        return $this->pocetX;
    }

    public function setPocetX(?int $pocetX): self
    {
        $this->pocetX = $pocetX;

        return $this;
    }

    public function getPocet8(): ?int
    {
        return $this->pocet8;
    }

    public function setPocet8(?int $pocet8): self
    {
        $this->pocet8 = $pocet8;

        return $this;
    }

    public function getPocet6(): ?int
    {
        return $this->pocet6;
    }

    public function setPocet6(?int $pocet6): self
    {
        $this->pocet6 = $pocet6;

        return $this;
    }

    public function getLast14(): ?float
    {
        return $this->last14;
    }

    public function setLast14(?float $last14): self
    {
        $this->last14 = $last14;

        return $this;
    }

    public function getLast7(): ?float
    {
        return $this->last7;
    }

    public function setLast7(?float $last7): self
    {
        $this->last7 = $last7;

        return $this;
    }

    public function getLast30(): ?float
    {
        return $this->last30;
    }

    public function setLast30(?float $last30): self
    {
        $this->last30 = $last30;

        return $this;
    }

    public function getAvgxp(): ?float
    {
        return $this->avgxp;
    }

    public function setAvgxp(?float $avgxp): self
    {
        $this->avgxp = $avgxp;

        return $this;
    }

    public function getPolniVelitel(): ?bool
    {
        return $this->polniVelitel;
    }

    public function setPolniVelitel(bool $polniVelitel): self
    {
        $this->polniVelitel = $polniVelitel;

        return $this;
    }

    public function getNaborar(): ?bool
    {
        return $this->naborar;
    }

    public function setNaborar(bool $naborar): self
    {
        $this->naborar = $naborar;

        return $this;
    }

    public function getTank1(): ?bool
    {
        return $this->tank_1;
    }

    public function setTank1(bool $tank_1): self
    {
        $this->tank_1 = $tank_1;

        return $this;
    }

    public function getTank2(): ?bool
    {
        return $this->tank_2;
    }

    public function setTank2(bool $tank_2): self
    {
        $this->tank_2 = $tank_2;

        return $this;
    }

    public function getTank3(): ?bool
    {
        return $this->tank_3;
    }

    public function setTank3(bool $tank_3): self
    {
        $this->tank_3 = $tank_3;

        return $this;
    }
}
