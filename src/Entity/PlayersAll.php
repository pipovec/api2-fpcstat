<?php

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\PlayersAllRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(description: 'Zoznam vsetkych hracov', operations: [
    new Get(),
    new GetCollection(),
], normalizationContext: [
    'groups' => [
        'players:read',
        'swagger_definition_name' => 'Read',
    ],
])]
#[ApiFilter(filterClass: SearchFilter::class, properties: ['nickname' => 'start'])]
#[ORM\Entity(repositoryClass: PlayersAllRepository::class)]
class PlayersAll
{
    #[Groups('players:read')]
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    private int $account_id;
    #[Groups('players:read')]
    #[ORM\Column(type: 'integer', nullable: true)]
    private int $clan_id;
    #[Groups('players:read')]
    #[ORM\Column(type: 'text')]
    private string $nickname;

    public function getAccountId(): ?int
    {
        return $this->account_id;
    }

    public function setAccountId(int $account_id): self
    {
        $this->account_id = $account_id;

        return $this;
    }

    public function getClanId(): ?int
    {
        return $this->clan_id;
    }

    public function setClanId(?int $clan_id): self
    {
        $this->clan_id = $clan_id;

        return $this;
    }

    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    public function setNickname(string $nickname): self
    {
        $this->nickname = $nickname;

        return $this;
    }
}
