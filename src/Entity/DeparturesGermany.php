<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use App\Repository\DeparturesGermanyRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: DeparturesGermanyRepository::class)]
#[ORM\Table(name: 'departures_germany')]
#[ApiResource(description: '', operations: [
    new Get(
        uriTemplate: '/departures-germany/{account_id}', normalizationContext: ['groups' => 'departure:de:read']
    ),
    new GetCollection(
        uriTemplate: '/departures-germany', normalizationContext: ['groups' => 'departure:de:read']
    ),
], cacheHeaders: [
    'max_age' => 3600,
    'shared_max_age' => 3600,
], paginationEnabled: false)]
#[ApiFilter(OrderFilter::class, properties: [
        'wn8',
        'global_rating',
        'role',
    ], arguments: ['orderParameterName' => 'order'])]
class DeparturesGermany
{
    #[ORM\Id]
    #[ORM\Column]
    #[Groups('departure:de:read')]
    private ?int $account_id = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups('departure:de:read')]
    private ?string $nickname = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups('departure:de:read')]
    private ?string $abbreviation = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups('departure:de:read')]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups('departure:de:read')]
    private ?string $role = null;

    #[ORM\Column]
    #[Groups('departure:de:read')]
    private ?float $wn8 = null;

    #[ORM\Column]
    #[Groups('departure:de:read')]
    private ?int $battles = null;

    #[ORM\Column]
    #[Groups('departure:de:read')]
    private ?int $wins = null;

    #[ORM\Column]
    #[Groups('departure:de:read')]
    private ?int $spotted = null;

    #[ORM\Column]
    #[Groups('departure:de:read')]
    private ?int $damage_dealt = null;

    #[ORM\Column]
    #[Groups('departure:de:read')]
    private ?int $dropped_capture_points = null;

    #[ORM\Column]
    #[Groups('departure:de:read')]
    private ?int $frags = null;

    #[ORM\Column]
    #[Groups('departure:de:read')]
    private ?int $global_rating = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups('departure:de:read')]
    private ?string $emblems_large = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups('departure:de:read')]
    private ?string $emblems_small = null;

    public function getAccountId(): ?int
    {
        return $this->account_id;
    }

    public function setAccountId(int $account_id): static
    {
        $this->account_id = $account_id;

        return $this;
    }

    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    public function setNickname(string $nickname): static
    {
        $this->nickname = $nickname;

        return $this;
    }

    public function getAbbreviation(): ?string
    {
        return $this->abbreviation;
    }

    public function setAbbreviation(string $abbreviation): static
    {
        $this->abbreviation = $abbreviation;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): static
    {
        $this->role = $role;

        return $this;
    }

    public function getWn8(): ?float
    {
        return $this->wn8;
    }

    public function setWn8(float $wn8): static
    {
        $this->wn8 = $wn8;

        return $this;
    }

    public function getBattles(): ?int
    {
        return $this->battles;
    }

    public function setBattles(int $battles): static
    {
        $this->battles = $battles;

        return $this;
    }

    public function getWins(): ?int
    {
        return $this->wins;
    }

    public function setWins(int $wins): static
    {
        $this->wins = $wins;

        return $this;
    }

    public function getSpotted(): ?int
    {
        return $this->spotted;
    }

    public function setSpotted(int $spotted): static
    {
        $this->spotted = $spotted;

        return $this;
    }

    public function getDamageDealt(): ?int
    {
        return $this->damage_dealt;
    }

    public function setDamageDealt(int $damage_dealt): static
    {
        $this->damage_dealt = $damage_dealt;

        return $this;
    }

    public function getDroppedCapturePoints(): ?int
    {
        return $this->dropped_capture_points;
    }

    public function setDroppedCapturePoints(int $dropped_capture_points): static
    {
        $this->dropped_capture_points = $dropped_capture_points;

        return $this;
    }

    public function getFrags(): ?int
    {
        return $this->frags;
    }

    public function setFrags(int $frags): static
    {
        $this->frags = $frags;

        return $this;
    }

    public function getGlobalRating(): ?int
    {
        return $this->global_rating;
    }

    public function setGlobalRating(int $global_rating): static
    {
        $this->global_rating = $global_rating;

        return $this;
    }

    public function getEmblemsLarge(): ?string
    {
        return $this->emblems_large;
    }

    public function setEmblemsLarge(string $emblems_large): static
    {
        $this->emblems_large = $emblems_large;

        return $this;
    }

    public function getEmblemsSmall(): ?string
    {
        return $this->emblems_small;
    }

    public function setEmblemsSmall(string $emblems_small): static
    {
        $this->emblems_small = $emblems_small;

        return $this;
    }
}
