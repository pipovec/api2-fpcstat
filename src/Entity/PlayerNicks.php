<?php
declare (strict_types = 1);

namespace App\Entity;


use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use App\State\PlayerNicksProvider;
use DateTimeInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Fully custom api with raw query in dataProvider
 *
 * @author   Boris Fekar
 * @package  App\Entity
 */
#[ApiResource(operations: [
    new GetCollection(
       uriTemplate: '/player_nicks/{account_id}', uriVariables: ['account_id'], provider: PlayerNicksProvider::class
    ),
], cacheHeaders: [
    'max_age'        => 86400,
    'shared_max_age' => 86400,
], normalizationContext: [
        'groups'                  => 'player-nick:read',
        'swagger_definition_name' => 'Read',
    ], paginationEnabled: false)]
class PlayerNicks
{
    public int $account_id;

    #[Groups('player-nick:read')]
    public string $nick;

    #[Groups('player-nick:read')]
    public DateTimeInterface $date;

    #[ApiProperty(identifier: true)]
    #[Groups('player-nick:read')]
    public function getIdentifier(): string
    {
        return $this->getAccountId() . '_' . $this->date->format('Y-m-d');
    }


    public function getAccountId(): int
    {
        return $this->account_id;
    }


    public function setAccountId(int $account_id): void
    {
        $this->account_id = $account_id;
    }


    public function getDate(): DateTimeInterface
    {
        return $this->date;
    }


    public function setDate(DateTimeInterface $date): void
    {
        $this->date = $date;
    }
}
