<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use App\Repository\AktualneOdchodyPlRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;


#[ApiResource(description: 'Odchody z Polskych klanov', operations: [
    new Get(uriTemplate: 'odchody-pl/{account_id}'),
    new GetCollection(uriTemplate: 'odchody-pl'),
], cacheHeaders: [
    'max_age'        => 3600,
    'shared_max_age' => 3600,
], normalizationContext: [
    'groups' => [
        'odchody_pl:read',
        'swagger_definition_name' => 'Read',
    ],
], paginationEnabled: false)]
#[ApiFilter(filterClass: SearchFilter::class, properties: ['nickname' => 'start', 'role' => 'exact'])]
#[ApiFilter(filterClass: RangeFilter::class, properties: ['wn8', 'global_rating'])]
#[ApiFilter(filterClass: OrderFilter::class, properties: ['wn8' => 'desc'])]
#[ORM\Table('aktualne_odchody_pl')]
#[ORM\Entity(repositoryClass: AktualneOdchodyPlRepository::class)]
class AktualneOdchodyPl
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    private ?int $account_id = null;

    /**
     * Nick hraca
     */
    #[Groups(['odchody_pl:read'])]
    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $nickname;

    /**
     * Skratka klanu
     */
    #[Groups(['odchody_pl:read'])]
    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $abbreviation;

    /**
     * Nazov klanu
     */
    #[Groups(['odchody_pl:read'])]
    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $name;

    /**
     * Velka ikona klanu (195x195)
     */
    #[Groups(['odchody_pl:read'])]
    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $emblems_large;

    /**
     * Mala ikona klanu (64x64)
     */
    #[Groups(['odchody_pl:read'])]
    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $emblems_small;

    /**
     * Rola v klane
     */
    #[Groups(['odchody_pl:read'])]
    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $role;

    /**
     * Aktualna hodnota WN8
     */
    #[Groups(['odchody_pl:read'])]
    #[ORM\Column(type: 'float', nullable: true)]
    private ?float $wn8;

    #[Groups(['odchody_pl:read'])]
    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $global_rating;

    #[Groups(['odchody_pl:read'])]
    #[ORM\OneToOne(targetEntity: PlayersStatAll::class, cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(name: 'account_id', referencedColumnName: 'account_id')]
    private ?PlayersStatAll $players_stat_all;

    public function getAccountId(): ?int
    {
        return $this->account_id;
    }

    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    public function getAbbreviation(): ?string
    {
        return $this->abbreviation;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function getWn8(): ?float
    {
        return $this->wn8;
    }


    public function getGlobalRating(): ?int
    {
        return $this->global_rating;
    }

    public function getPlayersStatAll(): ?PlayersStatAll
    {
        return $this->players_stat_all;
    }

    /**
     * @return string|null
     */
    public function getEmblemsLarge(): ?string
    {
        return $this->emblems_large;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }


    /**
     * @return string|null
     */
    public function getEmblemsSmall(): ?string
    {
        return $this->emblems_small;
    }
}
