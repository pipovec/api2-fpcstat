<?php

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\AktualneOdchodyRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;


#[ORM\Table('aktualne_odchody')]
#[ORM\Entity(repositoryClass: AktualneOdchodyRepository::class)]
#[ApiResource(description: 'Odchody z českých a slovenských klanov', operations: [
    new Get(uriTemplate: 'odchody_cz/{account_id}'),
    new GetCollection(uriTemplate: 'odchody_cz'),
], cacheHeaders: [
    'max_age'        => 3600,
    'shared_max_age' => 3600,
], normalizationContext: [
    'groups' => [
        'odchody_cz:read',
        'swagger_definition_name' => 'Read',
    ],
], paginationEnabled: false)]
#[ApiFilter(filterClass: SearchFilter::class, properties: ['nickname' => 'start', 'role' => 'exact'])]
#[ApiFilter(filterClass: RangeFilter::class, properties: ['wn8', 'global_rating'])]
#[ApiFilter(filterClass: OrderFilter::class, properties: ['wn8' => 'desc'])]
class AktualneOdchody
{
    #[Groups(['odchody_cz:read'])]
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    private ?int $account_id;
    /**
     * Nick hraca
     */
    #[Groups(['odchody_cz:read'])]
    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $nickname;
    /**
     * Skratka klanu
     */
    #[Groups(['odchody_cz:read'])]
    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $abbreviation;
    /**
     * Nazov klanu
     */
    #[Groups(['odchody_cz:read'])]
    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $name;
    /**
     * Velka ikona klanu (195x195)
     */
    #[Groups(['odchody_cz:read'])]
    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $emblems_large;
    /**
     * Mala ikona klanu (64x64)
     */
    #[Groups(['odchody_cz:read'])]
    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $emblems_small;
    /**
     * Rola v klane
     */
    #[Groups(['odchody_cz:read'])]
    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $role;
    /**
     * Aktualna hodnota WN8
     */
    #[Groups(['odchody_cz:read'])]
    #[ORM\Column(type: 'float', nullable: true)]
    private ?float          $wn8;
    #[Groups(['odchody_cz:read'])]
    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int            $global_rating;
    #[Groups(['odchody_cz:read'])]
    #[ORM\OneToOne(targetEntity: PlayersStatAll::class, cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(name: 'account_id', referencedColumnName: 'account_id')]
    private ?PlayersStatAll $players_stat_all;

    public function getAccountId(): ?int
    {
        return $this->account_id;
    }

    public function setAccountId(int $account_id): self
    {
        $this->account_id = $account_id;

        return $this;
    }

    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    public function setNickname(?string $nickname): self
    {
        $this->nickname = $nickname;

        return $this;
    }

    public function getAbbreviation(): ?string
    {
        return $this->abbreviation;
    }

    public function setAbbreviation(?string $abbreviation): self
    {
        $this->abbreviation = $abbreviation;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(?string $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getWn8(): ?float
    {
        return $this->wn8;
    }

    public function setWn8(?float $wn8): self
    {
        $this->wn8 = $wn8;

        return $this;
    }

    public function getGlobalRating(): ?int
    {
        return $this->global_rating;
    }

    public function setGlobalRating(?int $global_rating): self
    {
        $this->global_rating = $global_rating;

        return $this;
    }

    public function getPlayersStatAll(): ?PlayersStatAll
    {
        return $this->players_stat_all;
    }

    public function setPlayersStatAll(?PlayersStatAll $players_stat_all): self
    {
        $this->players_stat_all = $players_stat_all;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmblemsLarge(): ?string
    {
        return $this->emblems_large;
    }

    /**
     * @param string|null $emblems_large
     *
     * @return AktualneOdchody
     */
    public function setEmblemsLarge(?string $emblems_large): self
    {
        $this->emblems_large = $emblems_large;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return AktualneOdchody
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmblemsSmall(): ?string
    {
        return $this->emblems_small;
    }

    /**
     * @param string|null $emblems_small
     *
     * @return AktualneOdchody
     */
    public function setEmblemsSmall(?string $emblems_small): self
    {
        $this->emblems_small = $emblems_small;

        return $this;
    }
}
