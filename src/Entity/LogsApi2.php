<?php

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;

use App\Repository\LogsApi2Repository;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

#[ApiResource(operations: [new Get(), new GetCollection()])]
#[ORM\Entity(repositoryClass: LogsApi2Repository::class)]
class LogsApi2
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    private                     $id;
    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $time;
    #[ORM\Column(type: 'string', length: 500)]
    private ?string             $uri;
    #[ORM\Column(type: 'text')]
    private ?string             $clientIp;
    #[ORM\Column(type: 'text')]
    private ?string             $method;
    #[ORM\Column(type: 'text')]
    private ?string             $querystring;

    public function __construct()
    {
        $this->id = Uuid::uuid6();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTime(): ?\DateTimeInterface
    {
        return $this->time;
    }

    public function setTime(?\DateTimeInterface $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getUri(): ?string
    {
        return $this->uri;
    }

    public function setUri(string $uri): self
    {
        $this->uri = $uri;

        return $this;
    }

    public function getClientIp(): ?string
    {
        return $this->clientIp;
    }

    public function setClientIp(string $clientIp): self
    {
        $this->clientIp = $clientIp;

        return $this;
    }

    public function getMethod(): ?string
    {
        return $this->method;
    }

    public function setMethod(string $method): self
    {
        $this->method = $method;

        return $this;
    }

    public function getQuerystring(): ?string
    {
        return $this->querystring;
    }

    public function setQuerystring(string $querystring): self
    {
        $this->querystring = $querystring;

        return $this;
    }
}
