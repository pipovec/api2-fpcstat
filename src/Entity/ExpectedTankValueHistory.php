<?php

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\ExpectedTankValueHistoryRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Context;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;

#[ApiResource(operations: [
    new Get(),
    new GetCollection(),
], paginationClientItemsPerPage: true)]
#[ApiFilter(filterClass: DateFilter::class, properties: ['date' => 'exclude_null'])]
#[ApiFilter(filterClass: SearchFilter::class, properties: ['tank_id' => 'exact'])]
#[ApiFilter(filterClass: OrderFilter::class, properties: ['date' => 'desc'])]
#[ORM\Entity(repositoryClass: ExpectedTankValueHistoryRepository::class)]
class ExpectedTankValueHistory
{

    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ApiProperty(identifier: true)]
    private $uuid;

    #[ORM\Column(name: 'tank_id', type: 'integer')]
    private ?int $tank_id;

    #[ORM\Column(type: 'float')]
    private ?float $frag;

    #[ORM\Column(type: 'float')]
    private ?float $dmg;

    #[ORM\Column(type: 'float')]
    private ?float $spot;

    #[ORM\Column(type: 'float')]
    private ?float $def;

    #[ORM\Column(type: 'float')]
    private ?float $win;

    #[Context([DateTimeNormalizer::FORMAT_KEY => 'Y-m-d'])]
    #[ORM\Column(type: 'date')]
    private $date;

    public function getTankId(): ?int
    {
        return $this->tank_id;
    }

    public function setTankId(int $tank_id): self
    {
        $this->tank_id = $tank_id;

        return $this;
    }

    public function getFrag(): ?float
    {
        return $this->frag;
    }

    public function setFrag(float $frag): self
    {
        $this->frag = $frag;

        return $this;
    }

    public function getDmg(): ?float
    {
        return $this->dmg;
    }

    public function setDmg(float $dmg): self
    {
        $this->dmg = $dmg;

        return $this;
    }

    public function getSpot(): ?float
    {
        return $this->spot;
    }

    public function setSpot(float $spot): self
    {
        $this->spot = $spot;

        return $this;
    }

    public function getDef(): ?float
    {
        return $this->def;
    }

    public function setDef(float $def): self
    {
        $this->def = $def;

        return $this;
    }

    public function getWin(): ?float
    {
        return $this->win;
    }

    public function setWin(float $win): self
    {
        $this->win = $win;

        return $this;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }
}
