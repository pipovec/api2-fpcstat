<?php

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\PlayersStatAllRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(operations: [
    new Get(uriTemplate: 'players_stat_all/{account_id}'),
    new GetCollection(uriTemplate: 'players_stat_all'),
])]
#[ORM\Entity(repositoryClass: PlayersStatAllRepository::class)]
class PlayersStatAll
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    private ?int $account_id;
    #[Groups(['odchody_cz:read', 'odchody_pl:read'])]
    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $spotted;
    #[Groups(['odchody_cz:read', 'odchody_pl:read'])]
    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $battle_avg_xp;
    #[Groups(['odchody_cz:read', 'odchody_pl:read'])]
    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $wins;
    #[Groups(['odchody_cz:read', 'odchody_pl:read'])]
    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $battles;
    #[Groups(['odchody_cz:read', 'odchody_pl:read'])]
    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $damage_dealt;
    #[Groups(['odchody_cz:read', 'odchody_pl:read'])]
    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $hits_percents;
    #[Groups(['odchody_cz:read', 'odchody_pl:read'])]
    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $frags;
    #[Groups(['odchody_cz:read', 'odchody_pl:read'])]
    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $dropped_capture_points;

    public function getSpotted(): ?int
    {
        return $this->spotted;
    }

    public function setSpotted(?int $spotted): self
    {
        $this->spotted = $spotted;

        return $this;
    }

    public function getBattleAvgXp(): ?int
    {
        return $this->battle_avg_xp;
    }

    public function setBattleAvgXp(?int $battle_avg_xp): self
    {
        $this->battle_avg_xp = $battle_avg_xp;

        return $this;
    }

    public function getWins(): ?int
    {
        return $this->wins;
    }

    public function setWins(?int $wins): self
    {
        $this->wins = $wins;

        return $this;
    }

    public function getBattles(): ?int
    {
        return $this->battles;
    }

    public function setBattles(?int $battles): self
    {
        $this->battles = $battles;

        return $this;
    }

    public function getDamageDealt(): ?int
    {
        return $this->damage_dealt;
    }

    public function setDamageDealt(?int $damage_dealt): self
    {
        $this->damage_dealt = $damage_dealt;

        return $this;
    }

    public function getHitsPercents(): ?int
    {
        return $this->hits_percents;
    }

    public function setHitsPercents(?int $hits_percents): self
    {
        $this->hits_percents = $hits_percents;

        return $this;
    }

    public function getFrags(): ?int
    {
        return $this->frags;
    }

    public function setFrags(?int $frags): self
    {
        $this->frags = $frags;

        return $this;
    }

    public function getDroppedCapturePoints(): ?int
    {
        return $this->dropped_capture_points;
    }

    public function setDroppedCapturePoints(?int $dropped_capture_points): self
    {
        $this->dropped_capture_points = $dropped_capture_points;

        return $this;
    }

    public function getAccountId(): ?int
    {
        return $this->account_id;
    }

    public function setAccountId(int $account_id): self
    {
        $this->account_id = $account_id;

        return $this;
    }
}
