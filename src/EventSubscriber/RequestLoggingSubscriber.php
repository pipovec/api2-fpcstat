<?php

namespace App\EventSubscriber;

use App\Entity\LogsApi2;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class RequestLoggingSubscriber implements EventSubscriberInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @throws Exception
     */
    public function onRequestEvent(RequestEvent $event)
    {
        if (!$event->isMainRequest()) {
            return;
        }

        $this->entityManager->getConnection()->insert(
            'logs_api2',
            [
                'uri' => urldecode($event->getRequest()->getPathInfo()),
                'client_ip' => $event->getRequest()->getClientIp(),
                'method' => $event->getRequest()->getMethod(),
                'querystring' => urldecode($event->getRequest()->getQueryString())
            ]
        );
    }

    public static function getSubscribedEvents(): array
    {
        return [
            RequestEvent::class => 'onRequestEvent',
        ];
    }
}
