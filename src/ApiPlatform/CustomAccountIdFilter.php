<?php declare(strict_types=1);

namespace App\ApiPlatform;

use ApiPlatform\Doctrine\Orm\Filter\FilterInterface;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Boris Fekar
 * @package  App\ApiPlatform
 */
class CustomAccountIdFilter implements FilterInterface
{
    public const ACCOUNT_ID_FILTER_CONTEXT = 'custom_account_id_filter';

//    public function apply(Request|\Doctrine\ORM\QueryBuilder $request, bool $normalization, array $attributes, array &$context)
//    {
//        $account_id = $request->query->getInt('account_id');
//
//        if (!$account_id) {
//            return;
//        }
//
//        $context[self::ACCOUNT_ID_FILTER_CONTEXT] = $account_id;
//    }

    /**
     * @inheritDoc
     */
    public function getDescription(string $resourceClass): array
    {
        return [
            'account_id' => [
                'property' => null,
                'type' => 'int',
                'required' => true,
                'openapi' => [
                    'description' => 'Custom account ID filter',
                ]
            ]
        ];
    }

    public function apply(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, Operation $operation = null, array $context = []): void
    {

    }
}
