<?php declare(strict_types=1);

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Entity\PlayerNicks;
use DateTimeImmutable;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Cache\InvalidArgumentException;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;


/**
 * @author Boris Fekar
 */
class PlayerNicksProvider implements ProviderInterface
{

    private EntityManagerInterface $entityManager;
    private CacheInterface $cache;

    public function __construct(
        EntityManagerInterface $entityManager,
        CacheInterface $customApi
    )
    {
        $this->entityManager = $entityManager;
        $this->cache = $customApi;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): object|array|null
    {
        $account_id = $uriVariables['account_id'];
        $cacheKey = (string)$account_id;

        return $this->cache->get($cacheKey, function(ItemInterface $item) use ($account_id){
            $item->expiresAfter(86400);
            return $this->getData((int)$account_id);
        });
    }

    /**
     * @param int $account_id
     * @return array<PlayerNicks>
     * @throws Exception
     * @throws \Exception
     */
    private function getData(int $account_id): array
    {
        $stmt = $this->entityManager->getConnection()
            ->prepare("SELECT account_id, nick, timestamp FROM players_nick WHERE account_id = :account_id");
        $res = $stmt->executeQuery([':account_id' => $account_id])->fetchAllAssociative();

        $result = [];

        foreach ($res as $item) {
            $data = new PlayerNicks();

            $data->account_id = (int)$item['account_id'];
            $data->nick = $item['nick'];
            $data->date = new DateTimeImmutable($item['timestamp']);

            $result[] = $data;
        }

        return $result;
    }
}
