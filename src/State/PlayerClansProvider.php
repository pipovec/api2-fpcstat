<?php declare(strict_types=1);


namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Entity\PlayerClans;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

/**
 * @author    Boris Fekar
 * @createdAt 29. 9. 2021
 * @package   App\DataProvider
 */
class PlayerClansProvider implements ProviderInterface

{
    const QUERY = "SELECT DISTINCT(mr.clan_id),abbreviation,name,emblems_large,emblems_small,language::text "
                    . "FROM members_role mr "
                    . "JOIN clan_all ca ON mr.clan_id = ca.clan_id "
                    . "WHERE account_id = :account_id";

    private EntityManagerInterface $entityManager;
    private CacheInterface $cache;

    public function __construct(
        EntityManagerInterface $entityManager,
        CacheInterface $cache
    )
    {
        $this->entityManager = $entityManager;
        $this->cache = $cache;
    }


    public function provide(Operation $operation, array $uriVariables = [], array $context = []): object|array|null
    {
        $account_id = (int)$uriVariables['account_id'];
        $cacheKey = (string)$account_id;

        return $this->cache->get(
            $cacheKey, function (ItemInterface $item) use ($account_id) {
            $item->expiresAfter(86400);
            return $this->getData($account_id);
        });
    }

    private function getData(int $account_id)
    {
        $stmt = $this->entityManager->getConnection()->prepare(self::QUERY);

        $res = $stmt->executeQuery([':account_id' => $account_id])->fetchAllAssociative();

        $result = [];

        foreach ($res as $item) {

            $lang = '';

            if ($item['language']) {
                $lang = rtrim($item['language']);
            }

            $result[] = new PlayerClans(
                $item['clan_id'],
                $item['abbreviation'],
                $item['name'],
                $item['emblems_large'],
                $item['emblems_small'],
                $lang,
            );
        }

        return $result;
    }
}
