<?php

namespace App\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Entity\SearchingClan;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class SearchingClanStateProvider implements ProviderInterface
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function provide(Operation $operation, array $uriVariables = [], array $context = []): object|array|null
    {
        if ($operation instanceof CollectionOperationInterface) {
            return $this->allPlayers();
        }

        return null;
    }

    /**
     * @throws \Doctrine\DBAL\Exception
     */
    private function allPlayers(): array
    {
        $dateInThePast = $this->dateInThePast();

        $this->deleteOldRecords($dateInThePast);

        $stmt = $this->entityManager->getConnection()
            ->executeQuery(
                'SELECT sc.account_id, sc.nickname, sc.created_at, wn8, ((wins/battles::float) * 100) as winrate '
                .'FROM searching_clan sc '
                .'LEFT JOIN players_stat_all psa ON sc.account_id = psa.account_id '
                .'LEFT JOIN wn8player wn ON sc.account_id = wn.account_id '
                .'WHERE created_at > \'' .$dateInThePast. '\''
            );

        return $stmt->fetchAllAssociative();
    }

    private function dateInThePast(): string
    {
        $date = new DateTime('now');
        $date->sub(new \DateInterval('P10D'));

        return $date->format('Y-m-d h:i:s');
    }

    /**
     * @throws \Doctrine\DBAL\Exception
     */
    private function deleteOldRecords(string $dateInThePast): void
    {
        $this->entityManager->getConnection()
            ->executeQuery(
                'DELETE FROM searching_clan WHERE created_at < \'' . $dateInThePast . '\''
            );
    }
}
