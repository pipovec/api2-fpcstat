<?php declare(strict_types=1);


namespace App\Tests\Functional;


use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use Symfony\Component\HttpFoundation\Response;


/**
 * @author Boris Fekar
 * @createdAt 29. 7. 2021
 * @package  App\Tests\Functional
 */
class EncyclopediaVehiclesResourceTest extends ApiTestCase
{
    /**
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function testEncyclopediaVehicles()
    {
        $client = self::createClient();

        /* UNHAPPY scenario */
        $client->request('POST', '/api/encyclopedia_vehicles');
        $this->assertResponseStatusCodeSame(Response::HTTP_METHOD_NOT_ALLOWED);

        $client->request('DELETE', '/api/encyclopedia_vehicles');
        $this->assertResponseStatusCodeSame(Response::HTTP_METHOD_NOT_ALLOWED);

        $client->request('PUT', '/api/encyclopedia_vehicles');
        $this->assertResponseStatusCodeSame(Response::HTTP_METHOD_NOT_ALLOWED);

        /* HAPPY scenario */
        $client->request('GET', '/api/encyclopedia_vehicles');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK, "Get all vehicles");
        $this->assertResponseHeaderSame('Content-Type', 'application/ld+json; charset=utf-8');

        $client->request('GET', '/api/encyclopedia_vehicles', [
            'query' => [
                'level' => 10,
                'type' => 'heavyTank',
                'nation' => 'ussr'
            ]
        ]);
        $this->assertResponseStatusCodeSame(Response::HTTP_OK, 'Get filtered vehicles');
    }
}