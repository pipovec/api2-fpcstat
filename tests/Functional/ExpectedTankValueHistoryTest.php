<?php declare(strict_types=1);


namespace App\Tests\Functional;


use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Boris Fekar
 * @createdAt 8. 8. 2021
 * @package  App\Tests\Functional
 */
class ExpectedTankValueHistoryTest extends ApiTestCase
{
    public function testTwoWeeksValues()
    {
        $client = self::createClient();

        /** UNHAPPY scenario */
        $client->request('POST', '/api/expected_tank_value_histories');
        $this->assertResponseStatusCodeSame(Response::HTTP_METHOD_NOT_ALLOWED);

        $client->request('DELETE', '/api/expected_tank_value_histories');
        $this->assertResponseStatusCodeSame(Response::HTTP_METHOD_NOT_ALLOWED);

        $client->request('PUT', '/api/expected_tank_value_histories');
        $this->assertResponseStatusCodeSame(Response::HTTP_METHOD_NOT_ALLOWED);

        /** HAPPY SCENARIO */
        $client->request('GET', '/api/expected_tank_value_histories/9492684e-70b0-4765-ac53-6b8309286adf');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        $client->request('GET', '/api/expected_tank_value_histories', [
            'query' => [
                'vehicle.tank_id' => '1',
                'date%5Bafter%5D' => '2021-08-07',
                'order%5Bdate%5D' => 'DESC'
            ]
        ]);
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }
}