<?php declare(strict_types=1);


namespace App\Tests\Functional;


use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Boris Fekar
 * @createdAt 29. 7. 2021
 * @package  App\Tests\Functional
 */
class ExpectedTankValueResourceTest extends ApiTestCase
{
    public function testExpectedTankValue()
    {
        $client = self::createClient();

        /* UNHAPPY scenario */
        $client->request('POST', '/api/expected_tank_values/1');
        $this->assertResponseStatusCodeSame(Response::HTTP_METHOD_NOT_ALLOWED);

        $client->request('DELETE', '/api/expected_tank_values/1');
        $this->assertResponseStatusCodeSame(Response::HTTP_METHOD_NOT_ALLOWED);

        $client->request('PUT', '/api/expected_tank_values/1');
        $this->assertResponseStatusCodeSame(Response::HTTP_METHOD_NOT_ALLOWED);

        $client->request('GET', '/api/expected_tank_values');
        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);


        /* HAPPY scenario */
        $client->request('GET', '/api/expected_tank_values/1');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertResponseHeaderSame('Content-Type', 'application/ld+json; charset=utf-8');

    }
}