<?php declare(strict_types=1);


namespace App\Tests\Functional;


use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Boris Fekar
 * @createdAt 28. 8. 2021
 * @package  App\Tests\Functional
 */
class AktualneOdchodyTest extends ApiTestCase
{
    public function testAktualneOdchody()
    {
        $client = self::createClient();

        /* UNHAPPY scenario */
        $client->request('POST', '/api/odchody_cz');
        $this->assertResponseStatusCodeSame(Response::HTTP_METHOD_NOT_ALLOWED);

        $client->request('DELETE', '/api/odchody_cz');
        $this->assertResponseStatusCodeSame(Response::HTTP_METHOD_NOT_ALLOWED);

        $client->request('PUT', '/api/odchody_cz');
        $this->assertResponseStatusCodeSame(Response::HTTP_METHOD_NOT_ALLOWED);
        /* UNHAPPY scenario */

        /* HAPPY scenario */
        $client->request('GET', '/api/odchody_cz');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertResponseHeaderSame('Content-Type', 'application/ld+json; charset=utf-8');
        /* HAPPY scenario */
    }
}